INSERT
    OVERWRITE TABLE
        tmp_3_parent_child_tmp
            SELECT
                    a.parent_id
                    ,a.child_id
                    ,b.child_id
                FROM
                    tmp_3_parent_child a
                        LEFT JOIN tmp_3_parent_child b
                            ON a.child_id = b.parent_id