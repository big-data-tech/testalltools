INSERT
    OVERWRITE TABLE
        tmp_3_parent_child
                SELECT
                    parent_id
                    ,child_id
                FROM
                    (
                        SELECT
                                parent_min_id as parent_id
                                ,child_id
                            FROM
                                tmp_2_parent_child_tmp
                        UNION ALL
                        SELECT
                                a.child_id as parent_id
                                ,a.parent_id as child_id
                            FROM
                                tmp_2_parent_child a
                                    LEFT JOIN tmp_2_parent_child_tmp b
                                        ON a.parent_id = b.parent_min_id
                                        AND a.child_id = b.child_id
                            WHERE
                                b.parent_min_id IS NULL
                    ) u