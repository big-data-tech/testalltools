INSERT
OVERWRITE TABLE
        tmp_2_parent_child
            SELECT *
            FROM
            (
                SELECT parent_id,child1 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child1 IS NOT null
                    AND parent_id <> child1
                UNION ALL
                SELECT parent_id,child2 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child2 IS NOT null
                    AND parent_id <> child2
                UNION ALL
                SELECT parent_id,child3 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child3 IS NOT null
                    AND parent_id <> child3
                UNION ALL
                SELECT parent_id,child4 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child4 IS NOT null
                    AND parent_id <> child4
                UNION ALL
                SELECT parent_id,child5 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child5 IS NOT null
                    AND parent_id <> child5
                UNION ALL
                SELECT parent_id,child6 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child6 IS NOT null
                    AND parent_id <> child6
                UNION ALL
                SELECT parent_id,child7 as child
                    FROM tmp_1_parent_child
                    WHERE
                    child7 IS NOT null
                    AND parent_id <> child7
)FINAL