INSERT
    OVERWRITE TABLE
        tmp_2_parent_child
            SELECT
                    parent_id
                    ,child_id
                FROM
                    (
                        SELECT a.parent_id parent_id,a.child_id child_id 
                            FROM 
                                tmp_3_parent_child_tmp a LEFT JOIN tmp_3_parent_child_tmp b 
                                    ON a.parent_id = b.child_id AND b.child_child_id IS NOT NULL
                            WHERE b.parent_id IS NULL AND a.parent_id <> a.child_id
                        UNION ALL
                        SELECT
                                parent_id
                                ,child_child_id as child_id
                            FROM
                                tmp_3_parent_child_tmp
                            WHERE
                                child_child_id IS NOT null
                                and parent_id <> child_child_id
                    ) u
                GROUP BY
                    parent_id
                    ,child_id