INSERT
OVERWRITE TABLE
        tmp_1_parent_child
            SELECT
                    CASE
                        WHEN from_parent IS NOT null THEN from_parent
                        ELSE from_id
                    END AS parent_id
                    ,from_parent_child AS child1
                    ,from_id AS child2
                    ,from_child_id AS child3
                    ,to_parent AS child4
                    ,to_parent_child AS child5
                    ,to_id AS child6
                    ,to_child_id AS child7
                FROM
                    (
                        SELECT
                                from_parent.parent_id AS from_parent
                                ,from_parent_child.child_id AS from_parent_child
                                ,sync.from_id AS from_id
                                ,from_child.child_id AS from_child_id
                                ,to_parent.parent_id AS to_parent
                                ,to_parent_child.child_id AS to_parent_child
                                ,sync.to_id AS to_id
                                ,to_child.child_id AS to_child_id
                            FROM
                                sync_log sync
                                    LEFT JOIN cookie_relation from_parent
                                        ON sync.from_id = from_parent.child_id
                                    LEFT JOIN cookie_relation AS from_parent_child
                                        ON from_parent.child_id = from_parent_child.parent_id
                                    LEFT JOIN cookie_relation AS from_child
                                        ON sync.from_id = from_child.parent_id
                                    LEFT JOIN cookie_relation to_parent
                                        ON sync.to_id = to_parent.child_id
                                    LEFT JOIN cookie_relation AS to_parent_child
                                        ON to_parent.child_id = to_parent_child.parent_id
                                    LEFT JOIN cookie_relation AS to_child
                                        ON sync.to_id = to_child.parent_id
                    ) r