INSERT
    OVERWRITE TABLE
        cookie_relation_result
                    SELECT * 
                        FROM
                        (SELECT
                                parent_id
                                ,child_id
                            FROM
                                tmp_3_parent_child
                        UNION ALL
                        SELECT 
                                    a.parent_id as parent_id,
                                    a.child_id as child_id 
                                FROM
                                    cookie_relation a 
                                    LEFT JOIN tmp_4 b
                                        ON a.parent_id = b.ids 
                                    LEFT JOIN tmp_4 c 
                                        ON a.child_id = c.ids
                                WHERE 
                                    b.ids IS NULL 
                                    AND c.ids IS NULL
                        )FINAL