INSERT
    OVERWRITE TABLE
        tmp_2_parent_child_tmp
            SELECT
                    MIN(parent_id)
                    ,child_id
                FROM
                    tmp_2_parent_child
                GROUP BY
                    child_id