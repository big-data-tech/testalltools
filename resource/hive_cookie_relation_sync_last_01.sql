INSERT
    OVERWRITE TABLE
        tmp_4
            SELECT ids 
                FROM 
                (SELECT
                        parent_id AS ids
                    FROM
                        tmp_3_parent_child
                UNION ALL
                SELECT
                        child_id AS ids
                    FROM
                        tmp_3_parent_child
                )u
                GROUP BY ids