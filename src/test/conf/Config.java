//package test.conf;
//
//import org.apache.commons.lang.StringUtils;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//
//public class Config {
//	
//	public static String IMPALAD_HOST = "localhost";
//	static {
//		String host = System.getenv().get("IMPALAD_HOST").toString();
//		if (!StringUtils.isEmpty(host)) {
//			IMPALAD_HOST = host;
//		}
//	}
//	public static String HIVE_HOST = "localhost";
//	static {
//		String host = System.getenv().get("IMPALAD_HOST").toString();
//		if (!StringUtils.isEmpty(host)) {
//			HIVE_HOST = host;
//		}
//	}
//	public static final String IMPALAD_JDBC_PORT = "21050";
//	public static final String IMPALA_CONNECTION_URL = "jdbc:hive2://" + IMPALAD_HOST + ':' + IMPALAD_JDBC_PORT + "/gmo_pdmp;auth=noSasl";
//	public static final int MAX_IMPALA_CONCURRENT_QUERIES = 4;
//
//	public static final String HIVE_JDBC_PORT = "10000";
//	/* we set hive.exec.dynamic.partition.mode=nonstrict to enable using all dynamic keys for dynamic partitions */
//	public static final String HIVE_CONNECTION_URL = 
//			"jdbc:hive2://" + HIVE_HOST + ':' + HIVE_JDBC_PORT + "/gmo_pdmp?" +
//					"hive.exec.dynamic.partition.mode=nonstrict;" +
//					"parquet.compression=GZIP;";
//	public static final String HIVE_JDBC_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";
//	public static final String HIVE_USER = "hdfs";
//	public static final String HIVE_PASSWORD = "";
//	
//	public static String ADMIN_DB_HOST = "localhost";
//	static {
//		String host = System.getenv().get("ADMIN_DB_HOST");
//		if (!StringUtils.isEmpty(host)) {
//			ADMIN_DB_HOST = host;
//		}
//	}
//	public static final String ADMIN_DB_PORT = "3306";
//	public static String ADMIN_DB_USERNAME = "gmo_pdmp";
//	public static String ADMIN_DB_PASSWORD = "GQrzm1+Vd=1";
//	public static final String ADMIN_JDBC_DRIVER_NAME = "org.mariadb.jdbc.Driver";
//	public static final String ADMIN_DB_CONNECTION_URL = "jdbc:mariadb://" + ADMIN_DB_HOST + ':' + ADMIN_DB_PORT + "/pdmp_admin";
//
//	public static final String FLUME_ACCESS_LOGS = "/user/hive/warehouse/gmo_pdmp.db/flume_ap_access_log/ap_access_log.*";
//	public static final String FLUME_RAW_ACCESS_LOGS = "/user/hive/warehouse/gmo_pdmp.db/raw_ap_access_log";
//	
//	public static String NAMENODE = "127.0.0.1";
//	static {
//		String host = System.getenv().get("NAMENODE");
//		if (!StringUtils.isEmpty(host)) {
//			NAMENODE = host;
//		}
//	}
//	
//    public JdbcTemplate impalaTemplate() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource(IMPALA_CONNECTION_URL);
//		dataSource.setDriverClassName(HIVE_JDBC_DRIVER_NAME);
//		return new JdbcTemplate(dataSource);
//    }
//    
//    public JdbcTemplate hiveTemplate() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource(HIVE_CONNECTION_URL, HIVE_USER, HIVE_PASSWORD);
//		dataSource.setDriverClassName(HIVE_JDBC_DRIVER_NAME);
//		return new JdbcTemplate(dataSource);
//    }
//	
//	public JdbcTemplate adminTemplate() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource(ADMIN_DB_CONNECTION_URL, ADMIN_DB_USERNAME, ADMIN_DB_PASSWORD);
//		dataSource.setDriverClassName(ADMIN_JDBC_DRIVER_NAME);
//		return new JdbcTemplate(dataSource);
//	}
//	
//	public static final String URL_PARAM_COMPRESS_INTERMEDIATE = "hive.exec.compress.intermediate=";
//    public static final String URL_PARAM_COMPRESS_OUTPUT = "hive.exec.compress.output=";
//    public static final String URL_PARAM_OUTPUT_COMPRESSION_TYPE = "mapred.output.compression.type=";
//    public static final String URL_PARAM_OUTPUT_COMPRESSION_CODEC = "mapred.output.compression.codec=";
//    public static final String URL_PARAM_DYNAMIC_PARTITION = "hive.exec.dynamic.partition=";
//    public static final String URL_PARAM_DYNAMIC_MODE = "hive.exec.dynamic.partition.mode=";
//    public static final String URL_PARAM_GZIP = "org.apache.hadoop.io.compress.GzipCodec;";
//    public static final String URL_PARAM_SNAPPY = "org.apache.hadoop.io.compress.SnappyCodec;";
//
//    public static final String URL_PARAM_TRUE = "true;";
//    public static final String URL_PARAM_FALSE = "false;";
//    public static final String URL_PARAM_BLOCK = "BLOCK;";
//    public static final String URL_PARAM_NONSTRICT = "nonstrict;";
//
//    public static final String FILE_HANDLER_ID = "file_handler_id";
//    public static final String CREATED_DATETIME = "created_datetime";
//    public static final String MODIFIED_DATETIME = "modified_datetime";
//    public static final String CONTRACT_ID = "contract_id";
//    public static final String SERVICE_ID = "service_id";
//    public static final String COUNTRY_CODE = "country_code";
//    public static final String QUOTE_TYPE = "quote_type";
//    public static final String FILE_HANDLER_TYPE = "file_handler_type";
//    public static final String FILE_TYPE = "file_type";
//    public static final String FILE_FORMAT = "file_format";
//    public static final String COMPRESS_TYPE = "compress_type";
//    public static final String DELIMITER_CHARACTER = "delimiter_character";
//    public static final String NEWLINE_CHARACTER = "newline_character";
//    public static final String FILE_PREFIX = "file_prefix";
//    public static final String HIVE_TABLE_NAME = "hive_table_name";
//    public static final String FILE_COMMENT = "file_comment";
//
//    public static final String FILE_HANDLER_MASTER_SELECT_ALL = "SELECT * FROM file_handler_master";
//    public static final String TMP_DEL_LOAD = "LOAD DATA INPATH ? OVERWRITE INTO TABLE [rep1].[rep2] PARTITION(MODE = ?)";
//    public static final String DEL_TMP_INSERT = "INSERT OVERWRITE TABLE [rep1].[rep2] ([rep3]) PARTITION(mode=?) SELECT [rep4] FROM [rep5].[rep6] A LEFT OUTER JOIN [rep7].[rep8] B ON A.[rep9] = B.[rep10] AND B.mode=? WHERE B.[rep11] IS NULL";
//    public static final String ENTRY_INSERT = "INSERT OVERWRITE TABLE [rep1].[rep2] ([rep3]) PARTITION(mode=?) SELECT [rep4] FROM [rep5].[rep6] A GROUP BY A.[rep7]";
//    public static final String ENTRY_INSERT_FROM_PURE = "INSERT OVERWRITE TABLE [rep1].[rep2] ([rep3]) PARTITION(mode=?) SELECT [rep4] FROM [rep5].[rep6] WHERE mode=?";
//    public static final String FILE_COLUMN_MASTER_SELECT = "SELECT * FROM file_column_master WHERE file_handler_id = ? ORDER BY column_order";
//    public static final String REFRESH_ENTR = "REFRESH entrance_table";
//    public static final String REFRESH_MIDDLE = "REFRESH middle_entrance_table";
//    public static final String REFRESH_TBL = "REFRESH [rep]";
//    public static final String QUERY_ENTRANCE_INSERT = "INSERT OVERWRITE TABLE [rep1].middle_entrance_table PARTITION(service_id, file_prefix, mode) SELECT record, service_id, file_prefix, mode FROM [rep2].entrance_table";
//    public static final String QUERY_LOAD_PURE = "LOAD DATA INPATH ? OVERWRITE INTO TABLE [rep1].[rep2] PARTITION(MODE=?)";
//    public static final String QUERY_PARQUET_INCR = "INSERT OVERWRITE TABLE [rep1].[rep2] ([rep3]) PARTITION(mode=?) SELECT [rep4] FROM (SELECT * FROM [rep5].[rep6] WHERE mode=?) A FULL OUTER JOIN (SELECT * FROM [rep7].[rep8] WHERE mode=?) B ON A.[rep9] = B.[rep10]";
//    public static final String QUERY_PARQUET_INCR_USUAL = "INSERT OVERWRITE TABLE [rep1].[rep2] ([rep3]) PARTITION(mode=?) SELECT [rep4] FROM (SELECT [rep5] FROM (SELECT * FROM [rep6].[rep7] WHERE mode=?) A FULL OUTER JOIN (SELECT * FROM [rep8].[rep9] WHERE mode=?) B ON A.[rep10] = B.[rep11]) C LEFT OUTER JOIN (SELECT * FROM [rep12].[rep13] WHERE mode=?) D ON C.[rep14] = D.[rep15] where D.[rep16] is Null";
//    public static final String FILE_IMPORT_FULL = "full";
//    public static final String FILE_IMPORT_INCR = "incr";
//    public static final String FILE_IMPORT_INCR_DEL = "incr_del";
//    public static final String FILE_IMPORT_HOME_DIR = "/entrance/gmo_pdmp";
//    public static final String FILE_MIDDLE_DIR = "/user/hive/warehouse/gmo_pdmp.db/middle_entrance_table";
//    public static final String PARTITION_SERVICEID = "/service_id=[rep_srv]";
//    public static final String PARTITION_FILE_PREFIX = "/file_prefix=[rep_pref]";
//    public static final String PARTITION_MODE = "/mode=[rep_mode]";
//    public static final String SLASH = "/";
//    public static final String HIVE_DIR = "/user/hive/warehouse/gmo_pdmp.db/";
//    
//    public static final String ENTRY_HOME_DIR = "/user/hive/warehouse/[rep1].db/[rep2]";
//    public static final String ENTRY_VALID = "valid";
//    public static final String ENTRY_INVALID = "invalid";
//    public static final String DEL_TMP_NEW = "new";
//    public static final String DEL_TMP_OLD = "old";
//    
//    public static final String HIVE_CONNECTION_URL_NOARG = "jdbc:hive2://" + HIVE_HOST + ':' + HIVE_JDBC_PORT + "/gmo_pdmp";
//    public JdbcTemplate hiveTemplateToArgs(String[] args) {
//        StringBuffer url = new StringBuffer(HIVE_CONNECTION_URL_NOARG);
//        if (args != null && args.length != 0) {
//            url.append("?");
//            for (int i = 0; i < args.length; i++) {
//                url.append(args[i]);
//            }
//        }
//        DriverManagerDataSource dataSource = new DriverManagerDataSource(url.toString(), HIVE_USER, HIVE_PASSWORD);
//        dataSource.setDriverClassName(HIVE_JDBC_DRIVER_NAME);
//        return new JdbcTemplate(dataSource);
//    }
//    
//    public static final String DB_NAME = "gmo_pdmp";
//    
//    public static final Short CONTRACT_STATUS_VALID = 0;
//    public static final Short CONTRACT_STATUS_INVALID = 1;
//	public static final Short SERVICE_STATUS_VALID = 0;
//	public static final Short SERVICE_STATUS_TRIAL = 1;
//}
