package test.mock;

import static org.mockito.Mockito.when;
import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Static.class)
public class StaticClassExample {

	@SuppressWarnings("deprecation")
	@Test
	public void testMockStatic() {
		PowerMockito.mockStatic(Static.class);
		when(Static.getName()).thenReturn("ta"); // this is soo wrong ;)
		Assert.assertEquals("ta", Static.getName());
	}
}