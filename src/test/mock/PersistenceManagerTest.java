package test.mock;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.hadoop.conf.Configuration;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.jdbc.core.JdbcTemplate;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PersistenceManager.class )
public class PersistenceManagerTest {

        @Test
        public void testCreateDirectoryStructure_ok() throws Exception {
                final String path = "directoryPath";
//                File fileMock = createMock(File.class);
//
//                PersistenceManager tested = new PersistenceManager();
//
//                expectNew(File.class, path).andReturn(fileMock);
//
//                expect(fileMock.exists()).andReturn(false);
//                expect(fileMock.mkdirs()).andReturn(true);
//
//                replay(fileMock, File.class);
//
//                assertTrue(tested.createDirectoryStructure(path));
//
//                verify(fileMock, File.class);
        }
}