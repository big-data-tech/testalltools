//package test.mock;
//
//import static org.mockito.Matchers.any;
//import static org.mockito.Matchers.anyString;
//import static org.mockito.Matchers.argThat;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//public class MockitoTest  {
//
//	  // assume there is a class MyDatabase
//	  @Mock
//	  MyDatabase databaseMock;
//
//	  @Before
//	  protected void setUp() throws Exception {
//	    MockitoAnnotations.initMocks(this);
//	  }
//
//	  @Test
//	  public void testQuery()  {
//	    // assume there is a class called ClassToTest
//	    // which could be tested
//	    ClassToTest t  = new ClassToTest(databaseMock);
//
//	    // call a method
//	    boolean check = t.query("* from t");
//
//	    // test the return type
//	    assertTrue(check);
//
//	    // test that the query() method on the 
//	    // mock object was called
//	    Mockito.verify(mock).query("* from t");
//	  }
//	} 
