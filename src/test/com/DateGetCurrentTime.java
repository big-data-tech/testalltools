package test.com;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateGetCurrentTime {
	public static void main(String[] args) throws Exception {
		// Date
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		
		// Calendar
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		
		Calendar calNow = Calendar.getInstance();
		System.out.println(date.getTime());
		
		System.out.println(Calendar.getInstance().getTime());
		
	}
}
