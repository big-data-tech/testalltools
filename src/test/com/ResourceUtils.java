package test.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;

/**
 * リソースにアクセスするユーティリティ。
 *
 * @author Taketo Matsuura
 */
public class ResourceUtils {

    /** デフォルトで使用されるエンコード */
    private static final String DEFAULT_ENCODE = "UTF-8";

    /**
     * 指定されたレコードの内容を文字列で取得して返却する。
     * <p>
     * 改行は除去される。
     *
     * @param resourcePath
     * @return 取得された文字列
     * @throws IOException
     */
    public static String getResourceAsString(String resourcePath) throws IOException {
        return getResourceAsString(resourcePath, DEFAULT_ENCODE);
    }

    /**
     * 指定されたレコードの内容を文字列で取得して返却する。
     * <p>
     * 改行は除去される。
     *
     * @param resourcePath
     * @param charsetName
     * @return 取得された文字列
     * @throws IOException
     */
    public static String getResourceAsString(String resourcePath, String charsetName) throws IOException {

        InputStream is = null;
        BufferedReader br = null;
        try {
            is = ResourceUtils.class.getResourceAsStream(resourcePath);
            if (is == null) {
                throw new IOException("there is no resource [" + resourcePath + "]");
            }
            br = new BufferedReader(new InputStreamReader(is, charsetName));

            StringBuilder buf = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                buf.append(line);
            }
            return new String(buf);
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(br);
        }
    }

}