package test.com;

import java.util.*; 
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class HiveClient {
	public static String HIVE_HOST = "10.112.21.72";
	static {
		String host = (String) System.getenv().get("HIVE_HOST");
		if (!StringUtils.isEmpty(host)) {
			HIVE_HOST = host;
		}
	}
	public static final int MAX_IMPALA_CONCURRENT_QUERIES = 4;

	public static final String HIVE_JDBC_PORT = "10000";
	public static final String HIVE_CONNECTION_URL = 
			"jdbc:hive2://" + HIVE_HOST + ':' + HIVE_JDBC_PORT + "/pdmp?" +
					"hive.exec.dynamic.partition.mode=nonstrict;" +
					"parquet.compression=GZIP;";
	public static final String HIVE_USER = "hdfs";
	public static final String HIVE_PASSWORD = "";
	public static final String HIVE_JDBC_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";
	
	
	private static final String PRE_ANALYZE_SQL_01 = new String(
			"resource/hive_cookie_relation_sync_pre_01.sql");
	private static final String PRE_ANALYZE_SQL_02 = new String(
			"resource/hive_cookie_relation_sync_pre_02.sql");
	private static final String LOOP_ANALYZE_SQL_01 = new String(
			"resource/hive_cookie_relation_sync_loop_01.sql");
	private static final String LOOP_ANALYZE_SQL_02 = new String(
			"resource/hive_cookie_relation_sync_loop_02.sql");
	private static final String LOOP_ANALYZE_SQL_03 = new String(
			"resource/hive_cookie_relation_sync_loop_03.sql");
	private static final String LOOP_ANALYZE_SQL_04 = new String(
			"resource/hive_cookie_relation_sync_loop_04.sql");
	private static final String LAST_ANALYZE_SQL_01 = new String(
			"resource/hive_cookie_relation_sync_last_01.sql");
	private static final String LAST_ANALYZE_SQL_02 = new String(
			"resource/hive_cookie_relation_sync_last_02.sql");
	private static final String CHECK_END_LOOP_SQL = new String(
			"resource/hive_cookie_relation_sync_check_loop.sql");
	
	JdbcTemplate hiveTemplate = null;
	public static void main(String[] args) throws IOException{
		
//		String preAnalyzeQuery01 = getQuery(PRE_ANALYZE_SQL_01);
//		String preAnalyzeQuery02 = getQuery(PRE_ANALYZE_SQL_02);
//		
//		String loopAnalyzeQuery01 = getQuery(LOOP_ANALYZE_SQL_01);
//		String loopAnalyzeQuery02 = getQuery(LOOP_ANALYZE_SQL_02);
//		String loopAnalyzeQuery03 = getQuery(LOOP_ANALYZE_SQL_03);
//		String loopAnalyzeQuery04 = getQuery(LOOP_ANALYZE_SQL_04);
//		
//		String checkEndLoopQuery = getQuery(CHECK_END_LOOP_SQL);
//		
//		String lastQnalyzeQuery01 = getQuery(LAST_ANALYZE_SQL_01);
//		String lastQnalyzeQuery02 = getQuery(LAST_ANALYZE_SQL_02);
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource(HIVE_CONNECTION_URL, HIVE_USER, HIVE_PASSWORD);
		dataSource.setDriverClassName(HIVE_JDBC_DRIVER_NAME);

		JdbcTemplate hiveTemplate = new JdbcTemplate(dataSource);
		
		hiveTemplate.setDatabaseProductName("pdmp");
		
		hiveTemplate.execute(read("sql"));
		
//		int size = hiveTemplate.queryForInt("select count(*) from tmp_3_parent_child_tmp where child_child_id is null");
//		
//		System.out.println("Result: " + size);
		
//		List longta = hiveTemplate.queryForList("select * from tmp_3_parent_child_tmp where child_child_id is null");
//		System.out.println("Result: " + longta);
//		
//		Iterator itr= longta.iterator();
//		while(itr.hasNext()){
//			 System.out.println( "LIST" + itr.next());
//			 Map longMap = (Map) itr.next();
//			 System.out.println( "LIST parent_id :" + longMap.get("parent_id"));
//		}	
//		Date start = new Date();
		
//		System.out.println( "@PRE 01");
//		hiveTemplate.execute(preAnalyzeQuery01);
//		System.out.println( "@PRE 02");
//		hiveTemplate.execute(preAnalyzeQuery02);
//		
//		System.out.println( "@LOOP START");
//		boolean keepLoop = true;
//		while (keepLoop) {
//			System.out.println( "@LOOP 01");
//			hiveTemplate.execute(loopAnalyzeQuery01);
//			System.out.println( "@LOOP 02");
//			hiveTemplate.execute(loopAnalyzeQuery02);
//			System.out.println( "@LOOP 03");
//			hiveTemplate.execute(loopAnalyzeQuery03);
//			System.out.println( "@LOOP 04");
//			hiveTemplate.execute(loopAnalyzeQuery04);
//			System.out.println( "@LOOP CHECK");
//			int numberOfNullChild = hiveTemplate.queryForInt(checkEndLoopQuery);
//			keepLoop = numberOfNullChild != 0 ? true : false;
//		}
////		System.out.println( "@LAST 01");
//		hiveTemplate.execute(lastQnalyzeQuery01);
////		System.out.println( "@LAST 02");
//		hiveTemplate.execute(lastQnalyzeQuery02);
//		Date end = new Date();
//		long seconds = (start.getTime()-end.getTime())/1000;
//		System.out.println( "@END run in " + seconds + " seconds");
		
	}
	/**
	 * クエリをファイルから読み込む
	 * @param path
	 * @return
	 * @throws IOException
	 */
	static String getQuery(String path) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(path));
		String line = null;
		String query = new String();
		while ((line = reader.readLine()) != null) {
			query += line + "\n";
		}
		reader.close();
		return query;
	}
	static String read(String pro) {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");
//			input2 = new FileInputStream("config.properties2");

			// load a properties file
			prop.load(input);
//			prop.load(input2);

			// get the property value and print it out
//			System.out.println(prop.getProperty("hive_sql"));
//			System.out.println(prop.getProperty("long_pro"));
//			System.out.println(prop.getProperty("hive_sql").length());
			return prop.getProperty(pro);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
