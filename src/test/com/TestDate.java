package test.com;

import java.sql.Timestamp;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TestDate {
	public static void main(String[] args) throws Exception {
		String date = new String("29/May/2016:17:04:59+0900");
		SimpleDateFormat format = new SimpleDateFormat(
				"dd/MMM/yyyy:HH:mm:ssZ", Locale.ENGLISH);
		Date dt = format.parse(date);
		System.out.println("Time:" + dt.getTime());
		System.out.println("Time:" + dt);
		long longdt = 1559203499000L;
		Timestamp ts = new java.sql.Timestamp(longdt);
		System.out.println("Time:" + ts);
	}
}
