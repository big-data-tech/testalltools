package test.com;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TestReadFile {
	public static void main(String[] args) throws IOException  {
	BufferedReader reader = new BufferedReader(new FileReader("conf/text"));
	String line = null;
	while ((line = reader.readLine()) != null) {
	    // ...
		System.out.println(line);
	}
	}
}
