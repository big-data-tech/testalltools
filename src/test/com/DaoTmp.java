package test.com;

import java.sql.Timestamp;

public class DaoTmp {
	public Integer batchId;
	public String batchKey;
	public short statusCode;
	public Timestamp createdDatetime;

	public static final short NEW = 1;
	
	// Move finished flume files to suitable loading directory for hive "insert into"
	public static final short LOADED_RAW_DATA = 10;
	
	// Hive insert into Parquet tables
	public static final short INSERTED_PARQUET = 20;
	
	// Refresh metadata for impala
	public static final short REFRESHED_IMPALA = 30;
	
	public static final short FINISHED = 40;
}
